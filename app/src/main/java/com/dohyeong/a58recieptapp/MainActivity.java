package com.dohyeong.a58recieptapp;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bixolon.labelprinter.BixolonLabelPrinter;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
    // Name of the connected device
    private String mConnectedDeviceName = null;

    private AlertDialog mWifiDialog;
    private AlertDialog mPrinterInformationDialog;
    private AlertDialog mSetPrintingTypeDialog;
    private AlertDialog mSetMarginDialog;
    private AlertDialog mSetWidthDialog;
    private AlertDialog mSetLengthDialog;
    private AlertDialog mSetBufferModeDialog;
    private AlertDialog mSetSpeedDialog;
    private AlertDialog mSetDensityDialog;
    private AlertDialog mSetOrientationDialog;
    private AlertDialog mSetOffsetDialog;
    private AlertDialog mCutterPositionSettingDialog;
    private AlertDialog mAutoCutterDialog;
    private AlertDialog mGetCharacterSetDialog;

    private boolean mIsConnected;

    static BixolonLabelPrinter mBixolonLabelPrinter;

    private boolean checkedManufacture = false;

    public Handler m_hHandler = null;
    public BluetoothAdapter m_BluetoothAdapter = null;
    public BluetoothLeScanner mLEScanner = null;
    public ScanSettings settings = null;
    public List<ScanFilter> filters;
    public ArrayAdapter<String> adapter = null;
    public ArrayList<BluetoothDevice> m_LeDevices;


    EditText editCustomer;

    EditText editProduct1;
    EditText editProduct2;
    EditText editProduct3;
    EditText editProduct4;
    EditText editProduct5;
    EditText editProduct6;
    EditText editProduct7;
    EditText editProduct8;
    EditText editProduct9;
    EditText editProduct10;
    EditText editProduct11;
    EditText editProduct12;
    EditText editProduct13;
    EditText editProduct14;
    EditText editProduct15;

    EditText editQty1;
    EditText editQty2;
    EditText editQty3;
    EditText editQty4;
    EditText editQty5;
    EditText editQty6;
    EditText editQty7;
    EditText editQty8;
    EditText editQty9;
    EditText editQty10;
    EditText editQty11;
    EditText editQty12;
    EditText editQty13;
    EditText editQty14;
    EditText editQty15;

    EditText editPrice1;
    EditText editPrice2;
    EditText editPrice3;
    EditText editPrice4;
    EditText editPrice5;
    EditText editPrice6;
    EditText editPrice7;
    EditText editPrice8;
    EditText editPrice9;
    EditText editPrice10;
    EditText editPrice11;
    EditText editPrice12;
    EditText editPrice13;
    EditText editPrice14;
    EditText editPrice15;

    TextView total1;
    TextView total2;
    TextView total3;
    TextView total4;
    TextView total5;
    TextView total6;
    TextView total7;
    TextView total8;
    TextView total9;
    TextView total10;
    TextView total11;
    TextView total12;
    TextView total13;
    TextView total14;
    TextView total15;

    TextView totalAmount;

    Button btnPrint;

    String nameCus;
    String pd1;
    String pd2;
    String pd3;
    String pd4;
    String pd5;
    String pd6;
    String pd7;
    String pd8;
    String pd9;
    String pd10;
    String pd11;
    String pd12;
    String pd13;
    String pd14;
    String pd15;


    int p1 = 0;
    int p2 = 0;
    int p3 = 0;
    int p4 = 0;
    int p5 = 0;
    int p6 = 0;
    int p7 = 0;
    int p8 = 0;
    int p9 = 0;
    int p10 = 0;
    int p11 = 0;
    int p12 = 0;
    int p13 = 0;
    int p14 = 0;
    int p15 = 0;

    int q1 = 1;
    int q2 = 1;
    int q3 = 1;
    int q4 = 1;
    int q5 = 1;
    int q6 = 1;
    int q7 = 1;
    int q8 = 1;
    int q9 = 1;
    int q10 = 1;
    int q11 = 1;
    int q12 = 1;
    int q13 = 1;
    int q14 = 1;
    int q15 = 1;

    int t1;
    int t2;
    int t3;
    int t4;
    int t5;
    int t6;
    int t7;
    int t8;
    int t9;
    int t10;
    int t11;
    int t12;
    int t13;
    int t14;
    int t15;

    int tt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editCustomer = findViewById(R.id.editCustomer);

        editProduct1 = findViewById(R.id.editProduct1);
        editProduct2 = findViewById(R.id.editProduct2);
        editProduct3 = findViewById(R.id.editProduct3);
        editProduct4 = findViewById(R.id.editProduct4);
        editProduct5 = findViewById(R.id.editProduct5);
        editProduct6 = findViewById(R.id.editProduct6);
        editProduct7 = findViewById(R.id.editProduct7);
        editProduct8 = findViewById(R.id.editProduct8);
        editProduct9 = findViewById(R.id.editProduct9);
        editProduct10 = findViewById(R.id.editProduct10);
        editProduct11 = findViewById(R.id.editProduct11);
        editProduct12 = findViewById(R.id.editProduct12);
        editProduct13 = findViewById(R.id.editProduct13);
        editProduct14 = findViewById(R.id.editProduct14);
        editProduct15 = findViewById(R.id.editProduct15);


        editPrice1 = findViewById(R.id.editPrice1);
        editPrice2 = findViewById(R.id.editPrice2);
        editPrice3 = findViewById(R.id.editPrice3);
        editPrice4 = findViewById(R.id.editPrice4);
        editPrice5 = findViewById(R.id.editPrice5);
        editPrice6 = findViewById(R.id.editPrice6);
        editPrice7 = findViewById(R.id.editPrice7);
        editPrice8 = findViewById(R.id.editPrice8);
        editPrice9 = findViewById(R.id.editPrice9);
        editPrice10 = findViewById(R.id.editPrice10);
        editPrice11 = findViewById(R.id.editPrice11);
        editPrice12 = findViewById(R.id.editPrice12);
        editPrice13 = findViewById(R.id.editPrice13);
        editPrice14 = findViewById(R.id.editPrice14);
        editPrice15 = findViewById(R.id.editPrice15);

        editPrice1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editPrice1.getText().toString().length() > 0 || !editPrice1.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice1.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editPrice1.getText().toString().length() > 0 || !editPrice1.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice1.setText("0");
                    update();
                }
            }
        });

        editPrice2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editPrice2.getText().toString().length() > 0 || !editPrice2.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice2.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editPrice2.getText().toString().length() > 0 || !editPrice2.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice2.setText("0");
                    update();
                }
            }
        });
        editPrice3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editPrice3.getText().toString().length() > 0 || !editPrice3.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice3.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editPrice3.getText().toString().length() > 0 || !editPrice3.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice3.setText("0");
                    update();
                }
            }
        });

        editPrice4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editPrice4.getText().toString().length() > 0 || !editPrice4.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice4.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editPrice4.getText().toString().length() > 0 || !editPrice4.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice4.setText("0");
                    update();
                }
            }
        });
        editPrice5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editPrice5.getText().toString().length() > 0 || !editPrice5.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice5.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editPrice5.getText().toString().length() > 0 || !editPrice5.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice5.setText("0");
                    update();
                }
            }
        });
        editPrice6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editPrice6.getText().toString().length() > 0 || !editPrice6.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice6.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editPrice6.getText().toString().length() > 0 || !editPrice6.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice6.setText("0");
                    update();
                }
            }
        });

        editPrice7.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editPrice7.getText().toString().length() > 0 || !editPrice7.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice7.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editPrice7.getText().toString().length() > 0 || !editPrice7.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice7.setText("0");
                    update();
                }
            }
        });

        editPrice8.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editPrice8.getText().toString().length() > 0 || !editPrice8.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice8.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editPrice8.getText().toString().length() > 0 || !editPrice8.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice8.setText("0");
                    update();
                }
            }
        });

        editPrice9.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editPrice9.getText().toString().length() > 0 || !editPrice9.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice9.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editPrice9.getText().toString().length() > 0 || !editPrice9.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice9.setText("0");
                    update();
                }
            }
        });

        editPrice10.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editPrice10.getText().toString().length() > 0 || !editPrice10.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice10.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editPrice10.getText().toString().length() > 0 || !editPrice10.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice10.setText("0");
                    update();
                }
            }
        });


        editPrice11.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editPrice11.getText().toString().length() > 0 || !editPrice11.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice11.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editPrice11.getText().toString().length() > 0 || !editPrice11.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice11.setText("0");
                    update();
                }
            }
        });
        editPrice12.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editPrice12.getText().toString().length() > 0 || !editPrice12.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice12.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editPrice12.getText().toString().length() > 0 || !editPrice12.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice12.setText("0");
                    update();
                }
            }
        });

        editPrice13.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editPrice13.getText().toString().length() > 0 || !editPrice13.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice13.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editPrice13.getText().toString().length() > 0 || !editPrice13.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice13.setText("0");
                    update();
                }
            }
        });


        editPrice14.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editPrice14.getText().toString().length() > 0 || !editPrice14.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice14.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editPrice14.getText().toString().length() > 0 || !editPrice14.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice14.setText("0");
                    update();
                }
            }
        });

        editPrice15.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editPrice15.getText().toString().length() > 0 || !editPrice15.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice15.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editPrice15.getText().toString().length() > 0 || !editPrice15.getText().toString().equals("")) {
                    update();
                } else {
                    editPrice15.setText("0");
                    update();
                }
            }
        });


        editQty1 = findViewById(R.id.editQty1);
        editQty2 = findViewById(R.id.editQty2);
        editQty3 = findViewById(R.id.editQty3);
        editQty4 = findViewById(R.id.editQty4);
        editQty5 = findViewById(R.id.editQty5);
        editQty6 = findViewById(R.id.editQty6);
        editQty7 = findViewById(R.id.editQty7);
        editQty8 = findViewById(R.id.editQty8);
        editQty9 = findViewById(R.id.editQty9);
        editQty10 = findViewById(R.id.editQty10);
        editQty11 = findViewById(R.id.editQty11);
        editQty12 = findViewById(R.id.editQty12);
        editQty13 = findViewById(R.id.editQty13);
        editQty14 = findViewById(R.id.editQty14);
        editQty15 = findViewById(R.id.editQty15);

        editQty1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editQty1.getText().toString().length() > 0 || !editQty1.getText().toString().equals("")) {
                    update();
                } else {
                    editQty1.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editQty1.getText().toString().length() > 0 || !editQty1.getText().toString().equals("")) {
                    update();
                } else {
                    editQty1.setText("0");
                    update();
                }
            }
        });


        editQty2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editQty2.getText().toString().length() > 0 || !editQty2.getText().toString().equals("")) {
                    update();
                } else {
                    editQty2.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editQty2.getText().toString().length() > 0 || !editQty2.getText().toString().equals("")) {
                    update();
                } else {
                    editQty2.setText("0");
                    update();
                }
            }
        });

        editQty3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editQty3.getText().toString().length() > 0 || !editQty3.getText().toString().equals("")) {
                    update();
                } else {
                    editQty3.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editQty3.getText().toString().length() > 0 || !editQty3.getText().toString().equals("")) {
                    update();
                } else {
                    editQty3.setText("0");
                    update();
                }
            }
        });

        editQty4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editQty4.getText().toString().length() > 0 || !editQty4.getText().toString().equals("")) {
                    update();
                } else {
                    editQty4.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editQty4.getText().toString().length() > 0 || !editQty4.getText().toString().equals("")) {
                    update();
                } else {
                    editQty4.setText("0");
                    update();
                }
            }
        });


        editQty5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editQty5.getText().toString().length() > 0 || !editQty5.getText().toString().equals("")) {
                    update();
                } else {
                    editQty5.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editQty5.getText().toString().length() > 0 || !editQty5.getText().toString().equals("")) {
                    update();
                } else {
                    editQty5.setText("0");
                    update();
                }
            }
        });

        editQty6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editQty6.getText().toString().length() > 0 || !editQty6.getText().toString().equals("")) {
                    update();
                } else {
                    editQty6.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editQty6.getText().toString().length() > 0 || !editQty6.getText().toString().equals("")) {
                    update();
                } else {
                    editQty6.setText("0");
                    update();
                }
            }
        });
        editQty7.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editQty7.getText().toString().length() > 0 || !editQty7.getText().toString().equals("")) {
                    update();
                } else {
                    editQty7.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editQty7.getText().toString().length() > 0 || !editQty7.getText().toString().equals("")) {
                    update();
                } else {
                    editQty7.setText("0");
                    update();
                }
            }
        });


        editQty8.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editQty8.getText().toString().length() > 0 || !editQty8.getText().toString().equals("")) {
                    update();
                } else {
                    editQty8.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editQty8.getText().toString().length() > 0 || !editQty8.getText().toString().equals("")) {
                    update();
                } else {
                    editQty8.setText("0");
                    update();
                }
            }
        });

        editQty9.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editQty9.getText().toString().length() > 0 || !editQty9.getText().toString().equals("")) {
                    update();
                } else {
                    editQty9.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editQty9.getText().toString().length() > 0 || !editQty9.getText().toString().equals("")) {
                    update();
                } else {
                    editQty9.setText("0");
                    update();
                }
            }
        });

        editQty10.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editQty10.getText().toString().length() > 0 || !editQty10.getText().toString().equals("")) {
                    update();
                } else {
                    editQty10.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editQty10.getText().toString().length() > 0 || !editQty10.getText().toString().equals("")) {
                    update();
                } else {
                    editQty10.setText("0");
                    update();
                }
            }
        });
        editQty11.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editQty11.getText().toString().length() > 0 || !editQty11.getText().toString().equals("")) {
                    update();
                } else {
                    editQty11.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editQty11.getText().toString().length() > 0 || !editQty11.getText().toString().equals("")) {
                    update();
                } else {
                    editQty11.setText("0");
                    update();
                }
            }
        });

        editQty12.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editQty12.getText().toString().length() > 0 || !editQty12.getText().toString().equals("")) {
                    update();
                } else {
                    editQty12.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editQty12.getText().toString().length() > 0 || !editQty12.getText().toString().equals("")) {
                    update();
                } else {
                    editQty12.setText("0");
                    update();
                }
            }
        });

        editQty13.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editQty13.getText().toString().length() > 0 || !editQty13.getText().toString().equals("")) {
                    update();
                } else {
                    editQty13.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editQty13.getText().toString().length() > 0 || !editQty13.getText().toString().equals("")) {
                    update();
                } else {
                    editQty13.setText("0");
                    update();
                }
            }
        });


        editQty14.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editQty14.getText().toString().length() > 0 || !editQty14.getText().toString().equals("")) {
                    update();
                } else {
                    editQty14.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editQty14.getText().toString().length() > 0 || !editQty14.getText().toString().equals("")) {
                    update();
                } else {
                    editQty14.setText("0");
                    update();
                }
            }
        });

        editQty15.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (editQty15.getText().toString().length() > 0 || !editQty15.getText().toString().equals("")) {
                    update();
                } else {
                    editQty15.setText("0");
                    update();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editQty15.getText().toString().length() > 0 || !editQty15.getText().toString().equals("")) {
                    update();
                } else {
                    editQty15.setText("0");
                    update();
                }
            }
        });


        total1 = findViewById(R.id.txtTotal1);
        total2 = findViewById(R.id.txtTotal2);
        total3 = findViewById(R.id.txtTotal3);
        total4 = findViewById(R.id.txtTotal4);
        total5 = findViewById(R.id.txtTotal5);
        total6 = findViewById(R.id.txtTotal6);
        total7 = findViewById(R.id.txtTotal7);
        total8 = findViewById(R.id.txtTotal8);
        total9 = findViewById(R.id.txtTotal9);
        total10 = findViewById(R.id.txtTotal10);
        total11 = findViewById(R.id.txtTotal11);
        total12 = findViewById(R.id.txtTotal12);
        total13 = findViewById(R.id.txtTotal13);
        total14 = findViewById(R.id.txtTotal14);
        total15 = findViewById(R.id.txtTotal15);

        totalAmount = findViewById(R.id.txtAmount);

        btnPrint = findViewById(R.id.btnPrint);


//        Thread t = new Thread() {
//            @Override
//            public void run() {
//                while (!isInterrupted()) {
//                    try {
//                        Thread.sleep(10000);
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//
//                                p1 = Integer.parseInt(editPrice1.getText().toString());
//                                p2 = Integer.parseInt(editPrice2.getText().toString());
//                                p3 = Integer.parseInt(editPrice3.getText().toString());
//                                p4 = Integer.parseInt(editPrice4.getText().toString());
//                                p5 = Integer.parseInt(editPrice5.getText().toString());
//                                p6 = Integer.parseInt(editPrice6.getText().toString());
//                                p7 = Integer.parseInt(editPrice7.getText().toString());
//                                p8 = Integer.parseInt(editPrice8.getText().toString());
//                                p9 = Integer.parseInt(editPrice9.getText().toString());
//                                p10 = Integer.parseInt(editPrice10.getText().toString());
//                                p11 = Integer.parseInt(editPrice11.getText().toString());
//                                p12 = Integer.parseInt(editPrice12.getText().toString());
//                                p13 = Integer.parseInt(editPrice13.getText().toString());
//                                p14 = Integer.parseInt(editPrice14.getText().toString());
//                                p15 = Integer.parseInt(editPrice15.getText().toString());
//
//                                q1 = Integer.parseInt(editQty1.getText().toString());
//                                q2 = Integer.parseInt(editQty2.getText().toString());
//                                q3 = Integer.parseInt(editQty3.getText().toString());
//                                q4 = Integer.parseInt(editQty4.getText().toString());
//                                q5 = Integer.parseInt(editQty5.getText().toString());
//                                q6 = Integer.parseInt(editQty6.getText().toString());
//                                q7 = Integer.parseInt(editQty7.getText().toString());
//                                q8 = Integer.parseInt(editQty8.getText().toString());
//                                q9 = Integer.parseInt(editQty9.getText().toString());
//                                q10 = Integer.parseInt(editQty10.getText().toString());
//                                q11 = Integer.parseInt(editQty11.getText().toString());
//                                q12 = Integer.parseInt(editQty12.getText().toString());
//                                q13 = Integer.parseInt(editQty13.getText().toString());
//                                q14 = Integer.parseInt(editQty14.getText().toString());
//                                q15 = Integer.parseInt(editQty15.getText().toString());
//
//                                t1 = p1 * q1;
//                                t2 = p2 * q2;
//                                t3 = p3 * q3;
//                                t4 = p4 * q4;
//                                t5 = p5 * q5;
//                                t6 = p6 * q6;
//                                t7 = p7 * q7;
//                                t8 = p8 * q8;
//                                t9 = p9 * q9;
//                                t10 = p10 * q10;
//                                t11 = p11 * q11;
//                                t12 = p12 * q12;
//                                t13 = p13 * q13;
//                                t14 = p14 * q14;
//                                t15 = p15 * q15;
//
//                                tt = t1 + t2 + t3 + t4 + t5 + t6 + t7 + t8 + t9 + t10 + t11 + t12 + t13 + t14 + t15;
//
//                               total1.setText(String.valueOf(t1));
//                                total2.setText(String.valueOf(t2));
//                                total3.setText(String.valueOf(t3));
//                                total4.setText(String.valueOf(t4));
//                                total5.setText(String.valueOf(t5));
//                                total6.setText(String.valueOf(t6));
//                                total7.setText(String.valueOf(t7));
//                                total8.setText(String.valueOf(t8));
//                                total9.setText(String.valueOf(t9));
//                                total10.setText(String.valueOf(t10));
//                                total11.setText(String.valueOf(t11));
//                                total12.setText(String.valueOf(t12));
//                                total13.setText(String.valueOf(t13));
//                                total14.setText(String.valueOf(t14));
//                                total15.setText(String.valueOf(t15));
//
//
//                                totalAmount.setText(String.valueOf(tt));
//
//
//                            }
//                        });
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//            }
//        };
//
//        t.start();
        mBixolonLabelPrinter = new BixolonLabelPrinter(this, mHandler, null);

        final int ANDROID_NOUGAT = 24;
        if (Build.VERSION.SDK_INT >= ANDROID_NOUGAT) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


        btnPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                printSampleReceipt();

            }
        });
    }


    @Override
    public void onDestroy() {
        try {
            unregisterReceiver(mUsbReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        mBixolonLabelPrinter.disconnect();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mIsConnected) {
            menu.getItem(0).setEnabled(false);
            menu.getItem(1).setEnabled(false);
            menu.getItem(2).setEnabled(false);
            menu.getItem(3).setEnabled(false);
            menu.getItem(4).setEnabled(true);
        } else {
            menu.getItem(0).setEnabled(true);
            menu.getItem(1).setEnabled(true);
            menu.getItem(2).setEnabled(true);
            menu.getItem(3).setEnabled(true);
            menu.getItem(4).setEnabled(false);
        }
        return true;
    }

    private void update() {
        p1 = Integer.parseInt(editPrice1.getText().toString());
        p2 = Integer.parseInt(editPrice2.getText().toString());
        p3 = Integer.parseInt(editPrice3.getText().toString());
        p4 = Integer.parseInt(editPrice4.getText().toString());
        p5 = Integer.parseInt(editPrice5.getText().toString());
        p6 = Integer.parseInt(editPrice6.getText().toString());
        p7 = Integer.parseInt(editPrice7.getText().toString());
        p8 = Integer.parseInt(editPrice8.getText().toString());
        p9 = Integer.parseInt(editPrice9.getText().toString());
        p10 = Integer.parseInt(editPrice10.getText().toString());
        p11 = Integer.parseInt(editPrice11.getText().toString());
        p12 = Integer.parseInt(editPrice12.getText().toString());
        p13 = Integer.parseInt(editPrice13.getText().toString());
        p14 = Integer.parseInt(editPrice14.getText().toString());
        p15 = Integer.parseInt(editPrice15.getText().toString());

        q1 = Integer.parseInt(editQty1.getText().toString());
        q2 = Integer.parseInt(editQty2.getText().toString());
        q3 = Integer.parseInt(editQty3.getText().toString());
        q4 = Integer.parseInt(editQty4.getText().toString());
        q5 = Integer.parseInt(editQty5.getText().toString());
        q6 = Integer.parseInt(editQty6.getText().toString());
        q7 = Integer.parseInt(editQty7.getText().toString());
        q8 = Integer.parseInt(editQty8.getText().toString());
        q9 = Integer.parseInt(editQty9.getText().toString());
        q10 = Integer.parseInt(editQty10.getText().toString());
        q11 = Integer.parseInt(editQty11.getText().toString());
        q12 = Integer.parseInt(editQty12.getText().toString());
        q13 = Integer.parseInt(editQty13.getText().toString());
        q14 = Integer.parseInt(editQty14.getText().toString());
        q15 = Integer.parseInt(editQty15.getText().toString());

        t1 = p1 * q1;
        t2 = p2 * q2;
        t3 = p3 * q3;
        t4 = p4 * q4;
        t5 = p5 * q5;
        t6 = p6 * q6;
        t7 = p7 * q7;
        t8 = p8 * q8;
        t9 = p9 * q9;
        t10 = p10 * q10;
        t11 = p11 * q11;
        t12 = p12 * q12;
        t13 = p13 * q13;
        t14 = p14 * q14;
        t15 = p15 * q15;

        tt = t1 + t2 + t3 + t4 + t5 + t6 + t7 + t8 + t9 + t10 + t11 + t12 + t13 + t14 + t15;

        total1.setText(String.valueOf(t1));
        total2.setText(String.valueOf(t2));
        total3.setText(String.valueOf(t3));
        total4.setText(String.valueOf(t4));
        total5.setText(String.valueOf(t5));
        total6.setText(String.valueOf(t6));
        total7.setText(String.valueOf(t7));
        total8.setText(String.valueOf(t8));
        total9.setText(String.valueOf(t9));
        total10.setText(String.valueOf(t10));
        total11.setText(String.valueOf(t11));
        total12.setText(String.valueOf(t12));
        total13.setText(String.valueOf(t13));
        total14.setText(String.valueOf(t14));
        total15.setText(String.valueOf(t15));


        totalAmount.setText(String.valueOf(tt));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                mBixolonLabelPrinter.findBluetoothPrinters();
                break;

            case R.id.item2:
                if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
                    Toast.makeText(this, "BluetoothLE Not Supported", Toast.LENGTH_SHORT).show();
                    return false;
                }

                final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
                m_BluetoothAdapter = bluetoothManager.getAdapter();

                // Checks if Bluetooth is supported on the device.
                if (m_BluetoothAdapter == null) {
                    Toast.makeText(this, "Error Bluetooth Not Supported", Toast.LENGTH_SHORT).show();

                    return false;
                }

                if (Build.VERSION.SDK_INT >= 21) {
                    mLEScanner = m_BluetoothAdapter.getBluetoothLeScanner();
                    settings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_POWER).build();
                    //settings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_OPPORTUNISTIC).build();
                    filters = new ArrayList<ScanFilter>();
                }

                m_hHandler = new Handler();
                adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.select_dialog_singlechoice);
                m_LeDevices = new ArrayList<BluetoothDevice>();

                adapter.clear();
                m_LeDevices.clear();
                scanLeDevice(true);

                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MainActivity.this);
                alertBuilder.setIcon(R.drawable.ic_launcher_background);
                alertBuilder.setTitle("연결할 프린터를 선택하세요");

                alertBuilder.setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alertBuilder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        scanLeDevice(false);

                        BluetoothDevice btDevice = m_LeDevices.get(id);
                        mConnectedDeviceName = btDevice.getName();
                        mBixolonLabelPrinter.connect(btDevice.getAddress(), 1);

                        //m_strDeviceName = btDevice.getName();
                        //m_strDeviceAddress = btDevice.getAddress();
                        //m_TextViewMac.setText(m_strDeviceAddress);
                    }
                });
                alertBuilder.show();
                break;

            case R.id.item3:
                mBixolonLabelPrinter.findNetworkPrinters(3000);
                return true;

            case R.id.item4:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
                    mBixolonLabelPrinter.findUsbPrinters();
                }
                return true;

            case R.id.item5:
                mBixolonLabelPrinter.disconnect();
                return true;

        }
        return false;
    }

    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    public void scanLeDevice(final boolean bEnable) {
        if (bEnable) {
            // Stops scanning after a pre-defined scan period.
            m_hHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (Build.VERSION.SDK_INT < 21) {
                        m_BluetoothAdapter.stopLeScan(m_LeScanCallback);
                    } else {
                        mLEScanner.stopScan(mScanCallback);
                    }
                }
            }, 10000);

            if (Build.VERSION.SDK_INT < 21) {
                m_BluetoothAdapter.startLeScan(m_LeScanCallback);
            } else {
                mLEScanner.startScan(filters, settings, mScanCallback);
            }
        } else {
            if (Build.VERSION.SDK_INT < 21) {
                m_BluetoothAdapter.stopLeScan(m_LeScanCallback);
            } else {
                mLEScanner.stopScan(mScanCallback);
            }
        }
    }

    @SuppressLint("NewApi")
    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);

            Log.i("callbackType", String.valueOf(callbackType));
            Log.i("result", result.toString());
            BluetoothDevice btDevice = result.getDevice();

            if (!m_LeDevices.contains(btDevice)) {
                m_LeDevices.add(btDevice);
                adapter.add(btDevice.getName() + "\n" + btDevice.getAddress());
            } else {
                return;
            }
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);

            for (ScanResult sr : results) {
                Log.i("ScanResult - Results", sr.toString());
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);

            Log.e("Scan Failed", "Error Code: " + errorCode);
        }
    };

    public BluetoothAdapter.LeScanCallback m_LeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!m_LeDevices.contains(device)) {
                        m_LeDevices.add(device);
                        adapter.add(device.getName() + "\n" + device.getAddress());
                    }
                }
            });
        }
    };

    Intent intent;


//    private final void setStatus(int resId)
//    {
//        final ActionBar actionBar = getActionBar();
//        actionBar.setSubtitle(resId);
//    }

    private void printSampleReceipt() {
        long now = System.currentTimeMillis();
        Date date = new Date(now);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String getTime = sdf.format(date);
        nameCus = editCustomer.getText().toString();
        pd1 = editProduct1.getText().toString();
        pd2 = editProduct2.getText().toString();
        pd3 = editProduct3.getText().toString();
        pd4 = editProduct4.getText().toString();
        pd5 = editProduct5.getText().toString();
        pd6 = editProduct6.getText().toString();
        pd7 = editProduct7.getText().toString();
        pd8 = editProduct8.getText().toString();
        pd9 = editProduct9.getText().toString();
        pd10 = editProduct10.getText().toString();
        pd11 = editProduct11.getText().toString();
        pd12 = editProduct12.getText().toString();
        pd13 = editProduct13.getText().toString();
        pd14 = editProduct14.getText().toString();
        pd15 = editProduct15.getText().toString();
        String mm = "(공급받는자 용)";

        DecimalFormat myFormatter = new DecimalFormat("###,###");
        String formattedStringPrice = myFormatter.format(tt);

        for (int i = 0; i < 2; i++) {


            mBixolonLabelPrinter.drawBlock(10, 40, 800, 1250, BixolonLabelPrinter.BLOCK_OPTION_BOX, 4);
            mBixolonLabelPrinter.drawText("거  래  명  세  표", 220, 50, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
            mBixolonLabelPrinter.drawText(mm, 600, 55, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
            mBixolonLabelPrinter.drawBlock(10, 50, 800, 50, BixolonLabelPrinter.BLOCK_OPTION_LINE_OVERWRITING, 4);
            mBixolonLabelPrinter.drawText(getTime, 30, 110, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
            mBixolonLabelPrinter.drawText(nameCus + " 귀 하", 750, 110, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
            mBixolonLabelPrinter.drawBlock(10, 100, 800, 103, BixolonLabelPrinter.BLOCK_OPTION_BOX, 3);
            mBixolonLabelPrinter.drawText("사업자 등록번호 : 608-90-01973    상호 : 58번상회    성명 : 김중수  (인)", 30, 160, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
            mBixolonLabelPrinter.drawBlock(10, 150, 800, 153, BixolonLabelPrinter.BLOCK_OPTION_BOX, 3);
            mBixolonLabelPrinter.drawText("주소 : 창원시 마산합포구 내서읍 유통단지로33 농산물센타", 30, 210, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
            mBixolonLabelPrinter.drawBlock(10, 200, 800, 203, BixolonLabelPrinter.BLOCK_OPTION_BOX, 3);
            mBixolonLabelPrinter.drawText("업태 : 서비스, 도매                                  종목 : 청과물중개", 30, 260, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
            mBixolonLabelPrinter.drawBlock(10, 250, 800, 253, BixolonLabelPrinter.BLOCK_OPTION_BOX, 3);
            mBixolonLabelPrinter.drawText("  합계금액 :     " + formattedStringPrice + " 원 정", 30, 304, BixolonLabelPrinter.FONT_SIZE_KOREAN2, 2, 2, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
            mBixolonLabelPrinter.drawBlock(10, 300, 800, 303, BixolonLabelPrinter.BLOCK_OPTION_BOX, 3);
            mBixolonLabelPrinter.drawBlock(10, 355, 800, 353, BixolonLabelPrinter.BLOCK_OPTION_BOX, 3);
            mBixolonLabelPrinter.drawText("품  목", 80, 365, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
            mBixolonLabelPrinter.drawText("규  격", 230, 365, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
            mBixolonLabelPrinter.drawText("수 량", 350, 365, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
            mBixolonLabelPrinter.drawText("단  가", 440, 365, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
            mBixolonLabelPrinter.drawText("공 급 가 액 ", 600, 365, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
            mBixolonLabelPrinter.drawBlock(200, 355, 201, 1152, BixolonLabelPrinter.BLOCK_OPTION_BOX, 2); // 품목 오른쪽 라인
            mBixolonLabelPrinter.drawBlock(330, 355, 331, 1152, BixolonLabelPrinter.BLOCK_OPTION_BOX, 2); // 규격 오른쪽 라인
            mBixolonLabelPrinter.drawBlock(420, 355, 421, 1152, BixolonLabelPrinter.BLOCK_OPTION_BOX, 2); // 수량 오른쪽 라인
            mBixolonLabelPrinter.drawBlock(530, 355, 531, 1152, BixolonLabelPrinter.BLOCK_OPTION_BOX, 2);// 단가 오른쪽 라인
            mBixolonLabelPrinter.drawBlock(10, 400, 800, 402, BixolonLabelPrinter.BLOCK_OPTION_BOX, 3); // 품목 수량 밑 라인
            mBixolonLabelPrinter.drawBlock(10, 450, 800, 451, BixolonLabelPrinter.BLOCK_OPTION_BOX, 2);
            mBixolonLabelPrinter.drawBlock(10, 500, 800, 501, BixolonLabelPrinter.BLOCK_OPTION_BOX, 2);
            mBixolonLabelPrinter.drawBlock(10, 550, 800, 551, BixolonLabelPrinter.BLOCK_OPTION_BOX, 2);
            mBixolonLabelPrinter.drawBlock(10, 600, 800, 601, BixolonLabelPrinter.BLOCK_OPTION_BOX, 2);
            mBixolonLabelPrinter.drawBlock(10, 650, 800, 652, BixolonLabelPrinter.BLOCK_OPTION_BOX, 3); // 품목5 라인
            mBixolonLabelPrinter.drawBlock(10, 700, 800, 701, BixolonLabelPrinter.BLOCK_OPTION_BOX, 2);
            mBixolonLabelPrinter.drawBlock(10, 750, 800, 751, BixolonLabelPrinter.BLOCK_OPTION_BOX, 2);
            mBixolonLabelPrinter.drawBlock(10, 800, 800, 801, BixolonLabelPrinter.BLOCK_OPTION_BOX, 2);
            mBixolonLabelPrinter.drawBlock(10, 850, 800, 851, BixolonLabelPrinter.BLOCK_OPTION_BOX, 2);
            mBixolonLabelPrinter.drawBlock(10, 900, 800, 902, BixolonLabelPrinter.BLOCK_OPTION_BOX, 3); // 품목10 라인
            mBixolonLabelPrinter.drawBlock(10, 950, 800, 951, BixolonLabelPrinter.BLOCK_OPTION_BOX, 2);
            mBixolonLabelPrinter.drawBlock(10, 1000, 800, 1001, BixolonLabelPrinter.BLOCK_OPTION_BOX, 2);
            mBixolonLabelPrinter.drawBlock(10, 1050, 800, 1051, BixolonLabelPrinter.BLOCK_OPTION_BOX, 2);
            mBixolonLabelPrinter.drawBlock(10, 1100, 800, 1101, BixolonLabelPrinter.BLOCK_OPTION_BOX, 2);
            mBixolonLabelPrinter.drawBlock(10, 1150, 800, 1152, BixolonLabelPrinter.BLOCK_OPTION_BOX, 3); // 품목 15라인
            mBixolonLabelPrinter.drawText("인수자                (인) ", 260, 1180, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
            //mBixolonLabelPrinter.drawBlock(10,1200,800,1201,BixolonLabelPrinter.BLOCK_OPTION_BOX,2);
            if (q1 > 0) {

                mBixolonLabelPrinter.drawText(pd1, 30, 410, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText("박스 / 개", 210, 410, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText(editQty1.getText().toString(), 400, 410, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(editPrice1.getText().toString(), 510, 410, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(total1.getText().toString(), 780, 410, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);

            }
            if (q2 > 0) {

                mBixolonLabelPrinter.drawText(pd2, 30, 460, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText("박스 / 개", 210, 460, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText(editQty2.getText().toString(), 400, 460, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(editPrice2.getText().toString(), 510, 460, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(total2.getText().toString(), 780, 460, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);

            }
            if (q3 > 0) {

                mBixolonLabelPrinter.drawText(pd3, 30, 510, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText("박스 / 개", 210, 510, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText(editQty3.getText().toString(), 400, 510, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(editPrice3.getText().toString(), 510, 510, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(total3.getText().toString(), 780, 510, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
            }
            if (q4 > 0) {

                mBixolonLabelPrinter.drawText(pd4, 30, 560, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText("박스 / 개", 210, 560, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText(editQty4.getText().toString(), 400, 560, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(editPrice4.getText().toString(), 510, 560, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(total4.getText().toString(), 780, 560, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);

            }
            if (q5 > 0) {

                mBixolonLabelPrinter.drawText(pd5, 30, 610, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText("박스 / 개", 210, 610, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText(editQty5.getText().toString(), 400, 610, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(editPrice5.getText().toString(), 510, 610, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(total5.getText().toString(), 780, 610, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);

            }
            if (q6 > 0) {

                mBixolonLabelPrinter.drawText(pd6, 30, 660, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText("박스 / 개", 210, 660, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText(editQty6.getText().toString(), 400, 660, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(editPrice6.getText().toString(), 510, 660, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(total6.getText().toString(), 780, 660, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
            }
            if (q7 > 0) {
                mBixolonLabelPrinter.drawText(pd7, 30, 710, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText("박스 / 개", 210, 710, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText(editQty7.getText().toString(), 400, 710, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(editPrice7.getText().toString(), 510, 710, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(total7.getText().toString(), 780, 710, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
            }
            if (q8 > 0) {
                mBixolonLabelPrinter.drawText(pd8, 30, 760, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText("박스 / 개", 210, 760, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText(editQty8.getText().toString(), 400, 760, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(editPrice8.getText().toString(), 510, 760, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(total8.getText().toString(), 780, 760, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
  }
            if (q9 > 0) {

                mBixolonLabelPrinter.drawText(pd9, 30, 810, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText("박스 / 개", 210, 810, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText(editQty9.getText().toString(), 400, 810, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(editPrice9.getText().toString(), 510, 810, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(total9.getText().toString(), 780, 810, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
            }
            if (q10 > 0) {

                mBixolonLabelPrinter.drawText(pd10, 30, 860, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText("박스 / 개", 210, 860, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText(editQty10.getText().toString(), 400, 860, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(editPrice10.getText().toString(), 510, 860, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(total10.getText().toString(), 780, 860, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
            }
            if (q11 > 0) {

                mBixolonLabelPrinter.drawText(pd11, 30, 910, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText("박스 / 개", 210, 910, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText(editQty11.getText().toString(), 400, 910, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(editPrice11.getText().toString(), 510, 910, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(total11.getText().toString(), 780, 910, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
            }
            if (q12 > 0) {

                mBixolonLabelPrinter.drawText(pd12, 30, 960, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText("박스 / 개", 210, 960, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText(editQty12.getText().toString(), 400, 960, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(editPrice12.getText().toString(), 510, 960, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(total12.getText().toString(), 780, 960, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
            }

            if (q13 > 0) {

                mBixolonLabelPrinter.drawText(pd13, 30, 1010, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText("박스 / 개", 210, 1010, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText(editQty13.getText().toString(), 400, 1010, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(editPrice13.getText().toString(), 510, 1010, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(total13.getText().toString(), 780, 1010, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
            }
            if (q14 > 0) {

                mBixolonLabelPrinter.drawText(pd14, 30, 1060, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText("박스 / 개", 210, 1060, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText(editQty14.getText().toString(), 400, 1060, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(editPrice14.getText().toString(), 510, 1060, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(total14.getText().toString(), 780, 1060, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
            }
            if (q15 > 0) {

                mBixolonLabelPrinter.drawText(pd15, 30, 1110, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText("박스 / 개", 210, 1110, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
                mBixolonLabelPrinter.drawText(editQty15.getText().toString(), 400, 1110, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(editPrice15.getText().toString(), 510, 1110, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
                mBixolonLabelPrinter.drawText(total15.getText().toString(), 780, 1110, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_RIGHT);
            }

            //mBixolonLabelPrinter.drawText( "품 목", 120, 360, BixolonLabelPrinter.FONT_SIZE_KOREAN5, 1, 1, 0, BixolonLabelPrinter.ROTATION_NONE, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);


//        mBixolonLabelPrinter.drawText("75-C51", 50, 1200, BixolonLabelPrinter.FONT_SIZE_24, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//        mBixolonLabelPrinter.drawText("������", 35, 900, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//        mBixolonLabelPrinter.drawText("��â��", 85, 900, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//
//        mBixolonLabelPrinter.drawText("������ȣ 1026-1287-1927", 160, 1250, BixolonLabelPrinter.FONT_SIZE_KOREAN2, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//        mBixolonLabelPrinter.drawText("��������   2017/12/31", 190, 1250, BixolonLabelPrinter.FONT_SIZE_KOREAN2, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//        mBixolonLabelPrinter.draw1dBarcode("123456789012", 160, 900, BixolonLabelPrinter.BARCODE_CODE128, 2, 10, 60, BixolonLabelPrinter.ROTATION_270_DEGREES, BixolonLabelPrinter.HRI_NOT_PRINTED, 0);
//
//        mBixolonLabelPrinter.drawText("ȫ�浿                          010-1234-5678", 230, 1200, BixolonLabelPrinter.FONT_SIZE_KOREAN2, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//        mBixolonLabelPrinter.drawText("��� ������ ��â�� 3912���� �Ｚ���ع�����λ�", 260, 1200, BixolonLabelPrinter.FONT_SIZE_KOREAN2, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//        mBixolonLabelPrinter.drawText("����Ʈ 204 / 702ȣ", 290, 1200, BixolonLabelPrinter.FONT_SIZE_KOREAN2, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//        mBixolonLabelPrinter.drawText("����Ű ������                     02-468-4317", 330, 1200, BixolonLabelPrinter.FONT_SIZE_KOREAN2, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//        mBixolonLabelPrinter.drawText("����� ���α� ������ 361-6���� ����Ű�� 2��", 360, 1200, BixolonLabelPrinter.FONT_SIZE_KOREAN2, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//        mBixolonLabelPrinter.drawText("�ſ�", 410, 1200, BixolonLabelPrinter.FONT_SIZE_KOREAN2, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//        mBixolonLabelPrinter.drawText("����ƽ� GX-100", 440, 1200, BixolonLabelPrinter.FONT_SIZE_KOREAN2, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//
//        mBixolonLabelPrinter.drawText("31-C1", 30, 600, BixolonLabelPrinter.FONT_SIZE_24, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, true, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//        mBixolonLabelPrinter.drawText("������", 80, 600, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//        mBixolonLabelPrinter.drawText("��â��", 120, 600, BixolonLabelPrinter.FONT_SIZE_KOREAN6, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//
//        mBixolonLabelPrinter.drawText("ȫ�浿", 300, 600, BixolonLabelPrinter.FONT_SIZE_KOREAN1, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//        mBixolonLabelPrinter.drawText("010-1234-5678", 330, 550, BixolonLabelPrinter.FONT_SIZE_KOREAN1, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//
//        mBixolonLabelPrinter.drawText("��â�� 3912���� �Ｚ��", 400, 600, BixolonLabelPrinter.FONT_SIZE_KOREAN1, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//        mBixolonLabelPrinter.drawText("�ع�����λ����Ʈ 204", 430, 600, BixolonLabelPrinter.FONT_SIZE_KOREAN1, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//        mBixolonLabelPrinter.drawText("702ȣ", 460, 600, BixolonLabelPrinter.FONT_SIZE_KOREAN1, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//
//        mBixolonLabelPrinter.drawText("������ȣ 1026-1287-1927", 50, 400, BixolonLabelPrinter.FONT_SIZE_KOREAN2, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//        mBixolonLabelPrinter.drawText("�������� 2017/12/31", 80, 400, BixolonLabelPrinter.FONT_SIZE_KOREAN2, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//
//        mBixolonLabelPrinter.drawText("ȫ�浿 010-1234-5678", 130, 350, BixolonLabelPrinter.FONT_SIZE_KOREAN2, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//        mBixolonLabelPrinter.drawText("��â�� 3912���� �Ｚ��", 160, 400, BixolonLabelPrinter.FONT_SIZE_KOREAN2, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//        mBixolonLabelPrinter.drawText("�ع�����λ����Ʈ 204/702ȣ", 190, 400, BixolonLabelPrinter.FONT_SIZE_KOREAN2, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//
//        mBixolonLabelPrinter.drawText("����Ű ������", 220, 350, BixolonLabelPrinter.FONT_SIZE_KOREAN2, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//        mBixolonLabelPrinter.drawText("02-648-4317", 250, 300, BixolonLabelPrinter.FONT_SIZE_KOREAN2, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//        mBixolonLabelPrinter.drawText("����ƽ� GX-100", 280, 380, BixolonLabelPrinter.FONT_SIZE_KOREAN2, 1, 1, 0, BixolonLabelPrinter.ROTATION_270_DEGREES, false, false, BixolonLabelPrinter.TEXT_ALIGNMENT_NONE);
//
//        mBixolonLabelPrinter.drawQrCode("www.bixolon.com", 350, 400, BixolonLabelPrinter.QR_CODE_MODEL2, BixolonLabelPrinter.ECC_LEVEL_15, 4, BixolonLabelPrinter.ROTATION_270_DEGREES);

            mBixolonLabelPrinter.print(1, 1);
            mm = "(공급자 용)";
        }
    }

    private final void setStatus(CharSequence subtitle) {
        final ActionBar actionBar = getActionBar();
        actionBar.setSubtitle(subtitle);
    }

    @SuppressLint("HandlerLeak")
    private void dispatchMessage(Message msg) {
        switch (msg.arg1) {
            case BixolonLabelPrinter.PROCESS_GET_STATUS:
                byte[] report = (byte[]) msg.obj;
                StringBuffer buffer = new StringBuffer();
                if ((report[0] & BixolonLabelPrinter.STATUS_1ST_BYTE_PAPER_EMPTY) == BixolonLabelPrinter.STATUS_1ST_BYTE_PAPER_EMPTY) {
                    buffer.append("Paper Empty.\n");
                }
                if ((report[0] & BixolonLabelPrinter.STATUS_1ST_BYTE_COVER_OPEN) == BixolonLabelPrinter.STATUS_1ST_BYTE_COVER_OPEN) {
                    buffer.append("Cover open.\n");
                }
                if ((report[0] & BixolonLabelPrinter.STATUS_1ST_BYTE_CUTTER_JAMMED) == BixolonLabelPrinter.STATUS_1ST_BYTE_CUTTER_JAMMED) {
                    buffer.append("Cutter jammed.\n");
                }
                if ((report[0] & BixolonLabelPrinter.STATUS_1ST_BYTE_TPH_OVERHEAT) == BixolonLabelPrinter.STATUS_1ST_BYTE_TPH_OVERHEAT) {
                    buffer.append("TPH(thermal head) overheat.\n");
                }
                if ((report[0] & BixolonLabelPrinter.STATUS_1ST_BYTE_AUTO_SENSING_FAILURE) == BixolonLabelPrinter.STATUS_1ST_BYTE_AUTO_SENSING_FAILURE) {
                    buffer.append("Gap detection error. (Auto-sensing failure)\n");
                }
                if ((report[0] & BixolonLabelPrinter.STATUS_1ST_BYTE_RIBBON_END_ERROR) == BixolonLabelPrinter.STATUS_1ST_BYTE_RIBBON_END_ERROR) {
                    buffer.append("Ribbon end error.\n");
                }

                if (report.length == 2) {
                    if ((report[1] & BixolonLabelPrinter.STATUS_2ND_BYTE_BUILDING_IN_IMAGE_BUFFER) == BixolonLabelPrinter.STATUS_2ND_BYTE_BUILDING_IN_IMAGE_BUFFER) {
                        buffer.append("On building label to be printed in image buffer.\n");
                    }
                    if ((report[1] & BixolonLabelPrinter.STATUS_2ND_BYTE_PRINTING_IN_IMAGE_BUFFER) == BixolonLabelPrinter.STATUS_2ND_BYTE_PRINTING_IN_IMAGE_BUFFER) {
                        buffer.append("On printing label in image buffer.\n");
                    }
                    if ((report[1] & BixolonLabelPrinter.STATUS_2ND_BYTE_PAUSED_IN_PEELER_UNIT) == BixolonLabelPrinter.STATUS_2ND_BYTE_PAUSED_IN_PEELER_UNIT) {
                        buffer.append("Issued label is paused in peeler unit.\n");
                    }
                }
                if (buffer.length() == 0) {
                    buffer.append("No error");
                }
                Toast.makeText(getApplicationContext(), buffer.toString(), Toast.LENGTH_SHORT).show();
                break;

            case BixolonLabelPrinter.PROCESS_GET_INFORMATION_MODEL_NAME:
            case BixolonLabelPrinter.PROCESS_GET_INFORMATION_FIRMWARE_VERSION:
            case BixolonLabelPrinter.PROCESS_EXECUTE_DIRECT_IO:
                Toast.makeText(getApplicationContext(), (String) msg.obj, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BixolonLabelPrinter.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BixolonLabelPrinter.STATE_CONNECTED:
//                            setStatus(getString(R.string.title_connected_to, mConnectedDeviceName));

                            //printSampleReceipt();
                            mIsConnected = true;
                            invalidateOptionsMenu();
                            break;

                        case BixolonLabelPrinter.STATE_CONNECTING:
//                            setStatus(R.string.title_connecting);
                            break;

                        case BixolonLabelPrinter.STATE_NONE:
//                            setStatus(R.string.title_not_connected);

                            mIsConnected = false;
                            invalidateOptionsMenu();
                            break;
                    }
                    break;

                case BixolonLabelPrinter.MESSAGE_READ:
                    MainActivity.this.dispatchMessage(msg);
                    break;

                case BixolonLabelPrinter.MESSAGE_DEVICE_NAME:
                    mConnectedDeviceName = msg.getData().getString(BixolonLabelPrinter.DEVICE_NAME);
                    Toast.makeText(getApplicationContext(), mConnectedDeviceName, Toast.LENGTH_LONG).show();
                    break;

                case BixolonLabelPrinter.MESSAGE_TOAST:

                    Toast.makeText(getApplicationContext(), msg.getData().getString(BixolonLabelPrinter.TOAST), Toast.LENGTH_SHORT).show();
                    break;

                case BixolonLabelPrinter.MESSAGE_LOG:
                    Toast.makeText(getApplicationContext(), msg.getData().getString(BixolonLabelPrinter.LOG), Toast.LENGTH_SHORT).show();
                    break;

                case BixolonLabelPrinter.MESSAGE_BLUETOOTH_DEVICE_SET:
                    if (msg.obj == null) {
                        Toast.makeText(getApplicationContext(), "No paired device", Toast.LENGTH_SHORT).show();
                    } else {
                        DialogManager.showBluetoothDialog(MainActivity.this, (Set<BluetoothDevice>) msg.obj);
                    }
                    break;

                case BixolonLabelPrinter.MESSAGE_USB_DEVICE_SET:
                    if (msg.obj == null) {
                        Toast.makeText(getApplicationContext(), "No connected device", Toast.LENGTH_SHORT).show();
                    } else {
                        DialogManager.showUsbDialog(MainActivity.this, (Set<UsbDevice>) msg.obj, mUsbReceiver);
                    }
                    break;

                case BixolonLabelPrinter.MESSAGE_NETWORK_DEVICE_SET:
                    if (msg.obj == null) {
                        Toast.makeText(getApplicationContext(), "No connectable device", Toast.LENGTH_SHORT).show();
                    }
                    DialogManager.showNetworkDialog(MainActivity.this, (Set<String>) msg.obj);
                    break;

            }
        }
    };

    private BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                mBixolonLabelPrinter.connect();
                Toast.makeText(getApplicationContext(), "Found USB device", Toast.LENGTH_SHORT).show();
            } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                mBixolonLabelPrinter.disconnect();
                Toast.makeText(getApplicationContext(), "USB device removed", Toast.LENGTH_SHORT).show();
            }

        }
    };


}
